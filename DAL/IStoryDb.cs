﻿using BOL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IStoryDb
    {
        IQueryable<Story> GetAll();
        IQueryable<Story> GetStoriesByStatus(bool IsApproved);
        IQueryable<Story> GetById(int SSid);
        IQueryable<Story> GetByUserId(string id);
        Task<bool> Create(Story story);
        Task<bool> Update(Story story);
        Task<bool> Delete(int SSid);
        Task<bool> Approve(int SSid);
    }
}
