export interface Ssuser {
  userName?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
  dob?: Date;
  profilePicPath?: string;
}
